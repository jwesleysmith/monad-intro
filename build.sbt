name := "monads"

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  "org.scalacheck" %% "scalacheck"   % "1.12.2"
, "org.scalaz"     %% "scalaz-core"  % "7.1.2"
)

initialCommands := "import monads._, Monad._"

//initialCommands := "import scalaz._, Scalaz._, org.scalacheck._, monads._, Monad._"

scalacOptions ++= Seq( 
  "-deprecation",
  "-unchecked",
  "-feature",
  "-language:existentials",
  "-language:higherKinds")

resolvers += "bintray/non" at "http://dl.bintray.com/non/maven"

addCompilerPlugin("org.spire-math" %% "kind-projector" % "0.6.0")

// if your project uses multiple Scala versions, use this for cross building
addCompilerPlugin("org.spire-math" % "kind-projector" % "0.6.0" cross CrossVersion.binary)
package monads

trait Contravariant[F[_]] {
  def contramap[A, B](fa: F[A])(f: (B => A)): F[B]
}

object Contravariant {
  def apply[F[_]: Contravariant]: Contravariant[F] =
    implicitly[Contravariant[F]]

  import scalaz.Equal

  implicit object EqualContravariant extends Contravariant[Equal] {
    def contramap[A, B](eqa: Equal[A])(f: B => A): Equal[B] =
      new Equal[B] {
        def equal(b1: B, b2: B): Boolean =
          ???
      }
  }

  import scalaz.{ Order, Ordering }

  implicit object OrderContravariant extends Contravariant[Order] {
    def contramap[A, B](orda: Order[A])(f: B => A): Order[B] =
      new Order[B] {
        def order(b1: B, b2: B): Ordering =
          ???
      }
  }
}